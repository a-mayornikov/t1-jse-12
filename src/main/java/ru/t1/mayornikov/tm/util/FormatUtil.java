package ru.t1.mayornikov.tm.util;

import static ru.t1.mayornikov.tm.constant.FormatConst.*;

public final class FormatUtil {

    private static String renderToFormat(final long bytes, final double bytesScale, final String bytesAttribute) {
        return DECIMAL_FORMAT.format(bytes / bytesScale) + SEPARATOR + bytesAttribute;
    }

    public static String formatBytes(final long bytes) {
        if (bytes <= BYTES_K && bytes >= 0) return renderToFormat(bytes, 1, BYTES_NAME);
        if (bytes <= BYTES_M) return renderToFormat(bytes, BYTES_K, BYTES_K_NAME);
        if (bytes <= BYTES_G) return renderToFormat(bytes, BYTES_M, BYTES_M_NAME);
        if (bytes <= BYTES_T) return renderToFormat(bytes, BYTES_G, BYTES_G_NAME);
        if (bytes <= BYTES_P) return renderToFormat(bytes, BYTES_T,BYTES_T_NAME);
        else return renderToFormat(bytes, BYTES_P, BYTES_P_NAME);
    }

    private FormatUtil() {
    }

}