package ru.t1.mayornikov.tm.controller;

import ru.t1.mayornikov.tm.api.controller.ITaskController;
import ru.t1.mayornikov.tm.api.service.ITaskService;
import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.model.Task;
import ru.t1.mayornikov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeTaskStatus(id, status);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeTaskStatus(index, status);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatus(id, Status.IN_PROGRESS);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeTaskStatus(index, Status.IN_PROGRESS);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void completeTaskById() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatus(id, Status.COMPLETED);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeTaskStatus(index, Status.COMPLETED);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void showTasks() {
        System.out.println("[SHOW TASKS]");
        int index = 1;
        for(final Task task: taskService.findAll()) {
            System.out.println(index++ + ". " + task.getName());
        }
        if (index == 1) System.out.println("No one task found... ");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TO DO:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[DONE]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.remove(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.remove(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOne(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[DONE]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOne(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[DONE]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOne(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        task.setName(name);
        task.setDescription(description);
        System.out.println("[DONE]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOne(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        task.setName(name);
        task.setDescription(description);
        System.out.println("[DONE]");
    }

}