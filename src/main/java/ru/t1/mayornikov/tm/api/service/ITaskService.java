package ru.t1.mayornikov.tm.api.service;

import ru.t1.mayornikov.tm.api.repository.ITaskRepository;
import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.model.Project;
import ru.t1.mayornikov.tm.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository {
    Task update(String id, String name, String description);

    Task update(Integer index, String name, String description);

    Task changeTaskStatus(String id, Status status);

    Task changeTaskStatus(Integer index, Status status);

}